# Scopes

The scope helps in managing variable accessibilty.

```JavaScript
if (true){
    const hello = 'world'
    console.log(hello) // 'world'
}
console.log(hello) // throws ReferenceError
```

It can be of various types:

## 1. Block Scope

Any variable declared using `let` or `const` in a code block is block scoped.

```JavaScript
if (true){
    // if block scope
    const hello = 'world'
    console.log(hello) // 'world'
}
console.log(hello) // throws ReferenceError
```
## 2. Function Scope

The keyword `var` which is also used for declaring variables is not block scoped, instead it is only function scoped.

``` JavaScript
function hello() {
    // hello function scope
    var greet = 'Hello World!'
    console.log(greet) // 'Hello World'
}
hello()
console.log(greet) //throws ReferenceError
```
Keywords `let` and `const` are also function scoped.

## 3. Nested Scope

Scopes can be nested. Take a look at the following example:

```JavaScript
function hello() {
  // "hello" function scope (outer scope)
  const greet = 'Hello World!';
  if (true) {
    // "if" code block scope (inner scope)
    const name = 'Sanyam';
    console.log(greet); // 'Hello World!'
  }
  console.log(name); // throws ReferenceError
}
hello();
```
`if` code block scope is nested inside the `hello()` function scope. Scopes of any type (code block, function) can be nested.

The scope contained within another scope is named *inner scope*. In the example, if code block scope is an *inner scope* of `hello()` function scope.

The scope that wraps another scope is named *outer scope*.

## 4. Global Scope

The global scope is the outermost scope.

```JavaScript
// global scope
let counter = 1 // this variable can be accessed from anywhere
```
A variable declared inside the global scope is named global variable. Global variables are accessible from any scope.

# Closure

Closure are functions that access values outside their own curly braces.

The closure has:

- Access to its own scope (variables defined between its curly brackets)
- Access to the outer function’s variables
- Access to the global variables


```JavaScript
function outerFunc() {
    // the outer scope
    let outerVar = 'I am from outside!';
    return function innerFunc() {
        // the inner scope
        console.log(outerVar); // 'I am from outside!'
    }
}
const inner = outerFunc();
inner();
```

The `innerFunc()` is invoked outside `outerFunc()` scope. Still, JavaScript understands that `outerVar` inside `innerFunc()` corresponds to the variable `outerVar` of `outerFunc()`.

## Closures store references to the outer function’s variables

One important characteristic of closure is that outer variables can keep their states between multiple calls. That means value of the outer variables will be changed if you change it using inner function.

```JavaScript
function counter() {
    let counter = 0;

    function increaseCounter() {
        return counter += 1;
    };

    return increaseCounter;
}

let counterStart = counter();
console.log(counterStart()); // 1
console.log(counterStart()); // 2
console.log(counterStart()); // 3
```
## Private variables with closures

We can access private variables with the help of closures.


```JavaScript
function outerFunc() {
  // the outer scope
  let outerVar = 'I am from outside!';
  function innerFunc() {
    // the inner scope
    console.log(outerVar); // 'I am from outside!'
  }
  return innerFunc;
}
const inner = outerFunc();
inner();
```
# References

- [JavaScript Scope and Closures](https://css-tricks.com/javascript-scope-closures/)
- [The Ultimate Guide to Hoisting, Scopes, and Closures in JavaScript](https://ui.dev/ultimate-guide-to-execution-contexts-hoisting-scopes-and-closures-in-javascript)
- [Mozilla docs](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Closures)
- [Scopes and Closures In-depth](https://www.youtube.com/watch?v=O312eN5J2bc&list=PLqq-6Pq4lTTZ_LyvzfrndUOkIvOF4y-_c&index=1)










